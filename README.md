# mall-admin-vue

#### 介绍
VUE简易版电商后台管理，预览地址：http://111.229.105.117:8080 .

#### 软件架构
后端项目见 [mall-admin-spring-boot](https://https://gitee.com/SR_Lesson/springboot_mall_admin)
##### 使用技术

- vue2.6.11
- vue-router3.1.5
- axios0.19.2
- element-ui2.13.0
- echarts4.6.0

#### 使用说明

1.  npm install
2.  npm run serve

#### 演示界面
![用户管理](https://images.gitee.com/uploads/images/2020/0408/161414_a404fcb8_5551811.png "1.png")

![角色列表](https://images.gitee.com/uploads/images/2020/0408/161428_4b99798a_5551811.png "2.png")

![分配权限](https://images.gitee.com/uploads/images/2020/0408/161451_513940d5_5551811.png "3.png")

![分类参数-动态](https://images.gitee.com/uploads/images/2020/0408/161504_3b2405e9_5551811.png "4-1.png")

![分类参数-静态](https://images.gitee.com/uploads/images/2020/0408/161530_83ef3383_5551811.png "5.png")

![添加商品](https://images.gitee.com/uploads/images/2020/0408/161544_27c7ed13_5551811.png "6.png")

![上传图片](https://images.gitee.com/uploads/images/2020/0408/161636_a79a1eb4_5551811.png "7.png")

![查看物流](https://images.gitee.com/uploads/images/2020/0408/161651_9b190563_5551811.png "8.png")

![数据统计](https://images.gitee.com/uploads/images/2020/0408/161708_e2063534_5551811.png "9.png")
