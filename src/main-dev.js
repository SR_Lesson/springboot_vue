import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import qs from 'qs'
import TreeTable from 'vue-table-with-tree-grid'
import VueQuillEditor from 'vue-quill-editor'
import {resetForm} from "@/plugins/ruoyi.js"

// 导入进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'


import axios from 'axios'
// 设置访问的跟路径
axios.defaults.baseURL = 'http://127.0.0.1:8090/api'
axios.interceptors.request.use(config => {
    NProgress.start();
    config.headers.Authorization = window.sessionStorage.getItem('token');
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    if (config.method === 'post' || config.method === 'put') {
      config.data = qs.stringify({
        ...config.data
      })
    }
    return config;
})
//设置进度条
axios.interceptors.response.use(config => {
    NProgress.done();
    return config;
})
Vue.prototype.$http = axios
Vue.prototype.resetForm = resetForm
// 导入全局样式
import './assets/css/global.css'
// 导入字体图片
import './assets/fonts/iconfont.css'
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
//消息提示的环境配置，设置为开发环境或者生产环境
Vue.config.productionTip = false

Vue.component('tree-table', TreeTable);

Vue.use(VueQuillEditor)
Vue.filter('dateFormat', function(originVal) {
    const dt = new Date(originVal)

    const y = dt.getFullYear()
    const m = (dt.getMonth() + 1 + '').padStart(2, '0')
    const d = (dt.getDate() + '').padStart(2, '0')

    const hh = (dt.getHours() + '').padStart(2, '0')
    const mm = (dt.getMinutes() + '').padStart(2, '0')
    const ss = (dt.getSeconds() + '').padStart(2, '0')

    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
  })

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
