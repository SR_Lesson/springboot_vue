import Vue from 'vue'
import VueRouter from 'vue-router'

const Login = () => import(/* webpackChunkName: "login_home_welcome" */ '../components/Login.vue')
const Home = () => import(/* webpackChunkName: "login_home_welcome" */ '../components/Home.vue')
const Welcome = () => import(/* webpackChunkName: "login_home_welcome" */ '../components/Welcome.vue')

const Users = () => import(/* webpackChunkName: "Users_Rights_Roles" */ '../components/users/Users.vue')
const Rights = () => import(/* webpackChunkName: "Users_Rights_Roles" */ '../components/power/Rights.vue')
const Roles = () => import(/* webpackChunkName: "Users_Rights_Roles" */ '../components/power/Roles.vue')

const Params = () => import(/* webpackChunkName: "Goods_Params" */ '../components/goods/Params.vue')
const GoodsList = () => import(/* webpackChunkName: "Goods_Params" */ '../components/goods/GoodsList.vue')
const GoodsAdd = () => import(/* webpackChunkName: "Goods_Params" */ '../components/goods/GoodsAdd.vue')

const Orders = () => import(/* webpackChunkName: "Order_Report" */ '../components/order/Orders.vue')
const Reports = () => import(/* webpackChunkName: "Order_Report" */ '../components/report/Reports.vue')
// 工作流
const ModelList = () => import(/* webpackChunkName: "Order_Report" */ '../components/work/ModelList.vue')// 流程管理
const TableList = () => import(/* webpackChunkName: "Order_Report" */ '../components/work/TableList.vue')// 表单管理
const MyProcess = () => import(/* webpackChunkName: "Order_Report" */ '../components/work/MyProcess.vue')// 我的流程
const MyAgent = () => import(/* webpackChunkName: "Order_Report" */ '../components/work/MyAgent.vue')// 我的代办
const MyAgentOk = () => import(/* webpackChunkName: "Order_Report" */ '../components/work/MyAgentOk.vue')// 我的已办
const MyApply = () => import(/* webpackChunkName: "Order_Report" */ '../components/work/MyApply.vue')// 我的申请

Vue.use(VueRouter)

const routes = [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    {
        path: '/home',
        component: Home,
        redirect: '/welcome',
        children: [
            {path: '/modelList', component: ModelList},
            {path: '/tableList', component: TableList},
            {path: '/myProcess', component: MyProcess},
            {path: '/myAgent', component: MyAgent},
            {path: '/myAgentOk', component: MyAgentOk},
            {path: '/myApply', component: MyApply},
            {path: '/welcome', component: Welcome},
            {path: '/users', component: Users},
            {path: '/rights', component: Rights},
            {path: '/roles', component: Roles},
            {path: '/params', component: Params},
            {path: '/goods', component: GoodsList},
            {path: '/goods/add', component: GoodsAdd},
            {path: '/orders', component: Orders},
            {path: '/reports', component: Reports}
        ]
    },
]

const router = new VueRouter({
    routes
})
// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
    // to将要访问的路径
    // from代表从哪个路径跳转而来
    // next 是一个函数，表示放行
    // next() 放行， next('/login')强制跳转
    if (to.path === '/login') return next();
    const tokenStr = window.sessionStorage.getItem('token');
    if (!tokenStr) return next('/login');
    next();
});

export default router
